# Task 1. 
## Create an array
### Solve the task:

- [x] Create a class named `Student`, containing the fields: surname and initials, group number, 
academic performance (an array of five elements). 
- [x] Create an array of ten elements of this type. 
- [x] Add the ability to output the names and numbers of groups of students with grades equal to only 9 or 10.

# Task 2.
## Sorting an array
### Solve the task:

- [X] Create a Train class containing the fields: destination name, train number, departure time. 
- [x] Create data in an array of five Train type elements.
- [x] Add the ability to sort the array elements by train numbers.
- [x] Add the ability to sort the array by destination, and trains with the same destinations should be ordered by departure time.
- [x] Add the ability to display information about the train whose number is entered by the user. 

# Task 3.
## Create a class aggregating an array of type Customer
### Solve the task:

- [x] Create a Customer class, the specification of which is given below. 
- [x] Define constructors, set and get methods, and the toString() method. 
- [X] Create a second class aggregating an array of type Customer, with suitable constructors and methods. 
- [x] Set data selection criteria and output this data to the console.

Customer class: 
- id
- surname
- name
- address
- credit card number
- bank account number

Find and output:
1. list of buyers in alphabetical order;
2. a list of customers whose credit card number is in the specified interval

# Task 4.
## Create a class aggregating an array of type Book
### Solve the task:

- [ ] Create a Book class, the specification of which is given below.
- [ ] Define constructors, set and get methods, and the toString() method. 
- [ ] Create a second class aggregating an array of type Book, with suitable constructors and methods. 
- [ ] Set data selection criteria and output this data to the console.

Book: 
- id
- title
- author(s)
- publisher
- year of publication
- number of pages
- price
- type of binding

Find and output:

1. a list of books by a given author;
2. a list of books published by a given publisher;
3. a list of books released after a given year.