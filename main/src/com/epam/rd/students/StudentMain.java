package com.epam.rd.students;

public class StudentMain {
    public static void main(String[] args) {
        studentsDatabaseDemo();
    }

    public static void studentsDatabaseDemo() {
        Student smith = new Student("Smith", "John", 1, new int[]{9, 8, 10, 7, 9});
        Student johnson = new Student("Johnson", "Michael", 2, new int[]{10, 8, 9, 8, 9});
        Student williams = new Student("Williams", "David", 1, new int[]{8, 9, 10, 8, 10});
        Student brown = new Student("Brown", "James", 2, new int[]{10, 9, 9, 9, 10});
        Student davis = new Student("Davis", "William", 1, new int[]{9, 9, 9, 10, 10});
        Student rodriguez = new Student("Rodriguez", "Joseph", 3, new int[]{10, 9, 9, 9, 9});
        Student martinez = new Student("Martinez", "Jonathan", 1, new int[]{8, 10, 10, 8, 9});
        Student andersen = new Student("Andersen", "Christopher", 2, new int[]{9, 9, 8, 10, 10});
        Student garcia = new Student("Garcia", "Robert", 1, new int[]{9, 10, 9, 8, 8});
        Student miller = new Student("Miller", "Michael", 3, new int[]{8, 9, 8, 10, 9});

        Student[] initArray = new Student[] {
                smith, johnson, williams, brown, davis, rodriguez, martinez, andersen, garcia, miller
        };

        Database collegeDb = new Database();

        System.out.printf("Database is empty: %s\n", collegeDb.isEmpty());

        for (Student student: initArray) {
            collegeDb.add(student);
        }

        System.out.printf("Database is empty: %s\n", collegeDb.isEmpty());

        System.out.printf("Database contains %d entries.\n", collegeDb.size());

        System.out.println(collegeDb.find("Johnson", "Michael"));

        collegeDb.printDatabase();

        collegeDb.remove("Davis", "William");
        System.out.println();
        collegeDb.findByMinGrade(9);
        collegeDb.clear();

        System.out.println();
        collegeDb.printDatabase();
    }
}
