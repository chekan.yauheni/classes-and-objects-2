package com.epam.rd.customer;

import java.util.Objects;

public class Customer {
    private final int id;
    private final String surname;
    private final String name;
    private Address address;
    private int cardNumber;
    private String accountNumber;

    public Customer(int id, String surname, String name, Address address, int cardNumber, String accountNumber) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.address = address;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
    }

    public int getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int compareTo(Customer other) {
        if (this.surname.compareTo(other.surname) == 0) {
            return this.name.compareTo(other.name);
        }
        return this.surname.compareTo(other.surname);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (id != customer.id) return false;
        if (cardNumber != customer.cardNumber) return false;
        if (!surname.equals(customer.surname)) return false;
        if (!name.equals(customer.name)) return false;
        if (!Objects.equals(address, customer.address)) return false;
        return Objects.equals(accountNumber, customer.accountNumber);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + surname.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + cardNumber;
        result = 31 * result + (accountNumber != null ? accountNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return  String.format(
                "id: %s, name: %s, surname: %s\ncard No: %d\naccount No: %s\naddress: %s",
                id, name, surname, cardNumber, accountNumber, address
        );
    }
}
